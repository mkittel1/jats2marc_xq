(:+++++++++++++++++++++++++++++++++++++++++++++++++:)
(: JATS (Z39.96) Journal Archiving and Interchange :)
(: Conversion JATS to MARC21                       :)
(: @author: M. Kittelmann                          :)
(: Module: MARC21                                  :)
(:+++++++++++++++++++++++++++++++++++++++++++++++++:)

xquery version "1.0";
module namespace marc = "marc";

declare namespace xlink="http://www.w3.org/1999/xlink" ;
declare namespace oai="http://www.openarchives.org/OAI/2.0/";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace ali="http://www.niso.org/schemas/ali/1.0";


declare function marc:bdleader-zs()
as xs:string? 
{
  concat(
  '000 ',
  '00000',
  'n',
  'ab',  (: zs article:)
  ' a',
  '22',
  '00000',
  'm',
  'u',
  ' ',   (: 19 multipart resource level :)
  '4500',
    
  "&#10;" 
)
};


declare function marc:bd001( $id as xs:string?)
as xs:string? {
  concat(
  '001 ',
  $id,
  "&#10;"
)
};


declare function marc:bd003()
as xs:string?
{
  concat(    
  '003 ',
  'DE-628',     
  "&#10;"
  )
};


declare function marc:bd005()
as xs:string?
{
  let $current-date := xs:dateTime( current-dateTime() )
  return
  concat(    
  '005 ',
  format-dateTime( $current-date, "[Y,4][M,2][D,2][H,2][m,2][s,2].0" ),
  "&#10;"
  )
};




declare function marc:bd008( $id, $pub-year, $country_code, $language_code )
as xs:string?
{
  concat(
  '008 ',
  substring( string(current-date()), 3, 2),  (:yy:)
  substring( string(current-date()), 6, 2),  (:mm:)
  substring( string(current-date()), 9, 2),  (:dd:)
  
  (: date :)
  's',
  $pub-year,
  '    ',
  
  (: country code :)
   $country_code,  
  
  (: 18-34 continuing resource specific :)
  '|| ',
  'p|o',
  '||||||', (: 24-29 :)
  '   a|',
  
  
  (: language code :)
  $language_code,
  
  
  '|',
  'd',
  
  "&#10;" 
)
};


declare function marc:bd016( $zdb-id as xs:string? )
as xs:string? {
  if ( $zdb-id != '' )
  then
    concat(
      '016 ',
      '7 ',
      '\031a',
       $zdb-id,
       '\0312',
       'DE-600',
       "&#10;"
    )
  else
    trace( "", "TODO: ZDB ID Anreicherung!!" )  
};



declare function marc:bd022( $id as xs:string?, $issn as xs:string?)
as xs:string?
{
  if ($issn != '' ) then
  concat(
    '022   ',
    '\031a',
    normalize-space( string( $issn ) ) ,
    "&#10;"
  )
  else trace( "", concat( "Keine E-ISSN!", ' - ', $id ) )
};



declare function marc:bd776( $id as xs:string?, $issn-print as xs:string?)
as xs:string?
{
  concat(
    '776 0 ',
    '\031c',
    'Print',
    '\031x',
    normalize-space( string( $issn-print ) ) ,
    "&#10;" 
  )
}; 



declare function marc:bd024( $id as xs:string?, $doi as xs:string?)
as xs:string?
{
  if ($doi != '' ) then
  concat(
    "024 7 ",
    "\031a",
    $doi,
    "\0312doi",
    "&#10;"
  )
  else trace( "", concat( "Kein DOI!", ' - ', $id ) )
};


declare function marc:bd035( $id as xs:string?,
                             $other-id as xs:string?,
                             $org-code as xs:string? )
as xs:string?
{
  if ($other-id != '' ) then
  concat(
    '035   ',
    '\031a',
    if ($org-code != '') then
    concat(
      '(',
      $org-code,
      ')'
    )
    else() (:trace( "", concat( "No 035 org code given.", ' - ', $id ) ) :),
    normalize-space( string( $other-id ) ) ,
    "&#10;"
  )
  else trace( "", concat( "No 035 id given.", ' - ', $id ) )
};




declare function marc:bd084( $fid-kennzeichen, $fid-bib )
as xs:string? {  
  concat(
    '084   ',
    '\0312fid',
    '\031a',
    '[FID]',
    if ($fid-kennzeichen != '')
    then
      $fid-kennzeichen
    else
      trace( "", 'Kein gültiges FID-Kennzeichen angegeben!!' )
    , 
    '\031q',
    if ($fid-bib != '')
    then
      $fid-bib
    else
      trace( "", 'Keine gültige FID-Bib angegeben!! ')    
    ,
    "&#10;"
   )
};



declare function marc:first-author ( $id as xs:string?,
                                     $author-inverted-order as xs:string? )  
   as xs:string? 
{
   if ( $author-inverted-order != '' )
   then
     concat(
      '100 1 ',
      '\031a',  
      normalize-space( $author-inverted-order),
      "&#10;" 
      )
   else
     trace( "", concat( "No author given for current article!", ' - ', $id ) )   
 } ;
 
     (: if ($editor)
    then
    concat(
      '\031e',
      'edt' :)


declare function marc:authors-700 ( $other_authors_inverted )  
   as xs:string? {
       if (not( empty( $other_authors_inverted ) ) )
       then
       string-join( 
         for $aut in $other_authors_inverted
         return 
           concat(     
             '700 1 ',
             '\031a',
             (: if ($editor)
             then normalize-space( replace( $author, 'editor"*', '' ) )
             else :) 
             normalize-space( $aut ),
             '\0314',            
              (: if ($editor)
              then 'edt'
              else :) 
              'aut',
             "&#10;"
           )
     )
     else()
 };
 

 
declare function marc:bd245( $id as xs:string?,
                          $title as xs:string?, 
                          $subtitle as xs:string?, 
                          $authors_normal_order as xs:string? )
as xs:string? {
  let $k := substring-before( $title, ' ')
  return
  if ( $title != "" )
  then  
  concat(
  '245 0',
      if (
           $k = 'The' or
           $k = 'A' or
           $k = 'La' or 
           $k = 'Le' or
           $k = 'Les' or
           $k = "L'" or
           $k = 'De la' or
           $k = 'Du' or
           $k = "D'" or 
           $k = 'Une' or
           $k = 'Un' or
           $k = 'Il' or
           $k = 'Des' or
           $k = 'La' or 
           $k = 'El' or
           $k = 'Gli' or
           $k = 'Los' or
           $k = 'Las' or
           $k = 'Una' or
           $k = 'Un' or
           $k = 'Unas' or
           $k = 'Unos' or
           $k = 'O' or 
           $k = 'A' or
           $k = 'Os' or
           $k = 'As' or
           $k = 'Uma' or
           $k = 'Um' or
           $k = 'Umas' or
           $k = 'Uns' 
     )
      then
      string-length( $k ) + 1 (: number of nonfiling characters :)
      else '0',
  
  '\031a',
  string( normalize-space( $title ) ),
  if ( $subtitle != '' )
  then
    concat(
      '\031b',
      string( normalize-space( $subtitle ) )
    )
  else(),
  
  if ( $authors_normal_order )
  then
  concat(
  ' /\031c',  (: slash für ISBD :)
  $authors_normal_order
   )
   else(),

  "&#10;"
)
else trace( "", concat( "No title 245 given!", ' - ', $id ) )
};


(: not used for journals !! :)
(: declare function marc:bd260( $id as xs:string?, $x as node()*)
as xs:string? {
  concat(
  '260   ',
  '\031c',
  '20',
  substring($x/pubdate, 7),
  '-',
  substring($x/pubdate, 1,2),
  '-',
  substring($x/pubdate, 4,2),
  
  '\031f',
  $x/front/journal-meta/publisher,
  "&#10;" 
)
}; :)
 

declare function marc:bd300( $id as xs:string?,
                             $fpage as xs:string?, 
                             $lpage as xs:string?)
as xs:string? {
  if ( normalize-space($fpage) != ''  and normalize-space($lpage) != '' and 
       string( number( normalize-space( $fpage ))) != 'NaN'  and  string( number( normalize-space( $lpage ))) != 'NaN' )
  then
  let $page_count := string( number($lpage) - number($fpage) + 1 )
  return
    if ( string( number($page_count) ) != 'NaN' )
    then
      concat(
       '300   ',
       '\031a',
       abs(number($page_count)),
       ' p.',
       "&#010;"
       )
    else trace( "", concat( "No valid information on page count 300.", ' - ', $id ) )
};
 

declare function marc:bd500( $notes as node()*)
as xs:string? {
  if (not(empty($notes)))
  then
    for $note in $notes
    return
    concat(
    '500 ',
    '  ', (: Indicators :)
    '\031a',
    $note,
    "&#10;" 
    )
  else()   
}; 


declare function marc:bd520( $abstract as xs:string? ) 
as xs:string? {
      if ( $abstract != '' )
      then
      concat(
              '520 3 ',
              '\031b',
              normalize-space( string( $abstract ) ),
              "&#10;" 
       )
     else()
};
 
 
 
declare function marc:bd542( $copyright_holder as xs:string?,
                          $copyright_statement as xs:string?, 
                          $copyright_year as xs:string?,
                          $is_open_acess as xs:boolean? ) 
as xs:string? {
  string-join(
  concat(
  '542 1 ',
  if ($copyright_holder != '')
  then
    concat(
      '\031d',
      normalize-space( string( $copyright_holder ) )
    )
  else(),
  
  if ($copyright_statement != '')
  then
    concat(
      '\031f',
      normalize-space( string( $copyright_statement ) )
    )
  else(),
  
  if ($copyright_year != '')
  then
    concat(
      '\031g',
      normalize-space( string( $copyright_year ) )
    )
  else(),
  
  if ( $is_open_acess )
  then 
  concat(
    '\031n',
    'Open Access'
  )
  else(),
  "&#10;"
 )
 ) 
};
 
 
 
declare function marc:bd653( $keywords as node()* ) 
as xs:string? { 
    if (not(empty($keywords)))
    then
    string-join(
      for $kw in $keywords
      return
        concat(
          '653   ',
          '\031a',
          $kw,
          "&#10;"
        )
    )
  else()
}; 
 


declare function marc:bd773( $id as xs:string?,
                          $jtitle as xs:string?, 
                          $short_name as xs:string?, 
                          $g as xs:string?,
                          $volume as xs:string?, 
                          $issue as xs:string?, 
                          $fpage as xs:string?,
                          $date as xs:string?,
                          $issn as xs:string? )
{
  concat(  
  '773 0 ',
  '\031t',
  normalize-space($jtitle),  
  if ($short_name)
  then concat(
  '\031p',  
   normalize-space($short_name)
 ) else (), 
  '\031g',
  normalize-space( $g ), 
     
  if ($volume)
  then
  concat(
  '\031q',
  normalize-space($volume)
  )
  else trace( "", concat( "No volume count.", ' - ', $id ) ),
 
  if ($issue)
  then
    concat(
      ':',
      normalize-space($issue)
    )
  else trace( "", concat( "No issue count.", ' - ', $id ) ),  
  
  if ( $fpage )
  then 
    concat(
      '<',
      normalize-space($fpage)
    )
  else(),
  
  '\0317unas',  
  if ( $issn and string-length( normalize-space($issn) ) > 4 )
  then 
    concat(
      '\031x',
      normalize-space($issn)
    )
   else(), 
  '\031d',
  normalize-space( $date ),  
  "&#10;" 

  )
};



declare function marc:bd774( $issue_title ) 
as xs:string? {
  if ( $issue_title != '' )
  then
  concat(
    '774 1 ',
    '\031a',
    $issue_title,
    '\031c',
    'Issue Title',
    "&#10;"
  )
  else()
};
 
 
declare function marc:bd856( $id as xs:string?,
                          $url as xs:string?,
                          $fid_url as xs:string?,
                          $proxy_url as xs:string?,
                          $han as xs:boolean?,
                          $fid_kennzeichen as xs:string?,
                          $is_nationallizenz as xs:boolean?,
                          $file ) 
as xs:string? {  
  concat(
    
    (: link to digital obj :)
    (:
    if (not(empty($filename)))
    then 
      concat(
     '856 70',
    '\031f',
    $file,
    '\031q',
    'application/pdf',
    '\0312file',
    "&#10;")
    else trace( "", concat( "Filename missing!", ' - ', $id ) )
    , :)
    
    '856 40',
    if ( $url != "" )
    then 
    concat(
      '\031u',
      $url
    )
    else trace( "", concat( "No URL given!", ' - ', $id ) ),
    '\031x',
    'Link to publisher',
    '\031y',
    "Verlagsseite",   
    "&#10;",
    
    if (not($is_nationallizenz)) (: kein Proxy-Link, wenn Nationallizenz :)
    then 
      concat(
        '856 40',
        '\031m',
        $fid_url,
        '\031u',
        if ($proxy_url != "")
        then
          $proxy_url
        else trace( "", concat( "Proxy URL missing", ' - ', $id ) ),
        if ($han )
        then 
          replace( $url, 'https*://', '') 
        else
          $url,
        '\031x',
        'Zugang für den berechtigten Nutzerkreis des FID ',
        $fid_kennzeichen,
        '\031y',
        'FID Access',     
        '\0313',
        'Full text',      (: cf. 009P/05 $3 34 http://swbtools.bsz-bw.de/cgi-bin/help.pl?cmd=kat&val=4085&regelwerk=RDA&verbund=GBV#$3 :)
        "&#10;"
      )
      else()    
  )
};

