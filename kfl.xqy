(:+++++++++++++++++++++++++++++++++++++++++++++++++:)
(: JATS (Z39.96) Journal Archiving and Interchange :)
(: Conversion JATS to MARC21                       :)
(: @author: M. Kittelmann                          :)
(: Module: KfL                                     :)
(:+++++++++++++++++++++++++++++++++++++++++++++++++:)


xquery version "1.0";
module namespace kfl = "kfl";


(:++++++++++++++++++++:)
(: set values         :)
(:++++++++++++++++++++:)


declare variable $kfl:supplier-org-code := "" ;

(: 856 :)
declare variable $kfl:han := false() ;
declare variable $kfl:proxy_url := 'http://fid-pharmazie.idm.oclc.org/login?url=' ;   (: bei Zeitschriften :)
declare variable $kfl:fid_kennzeichen := 'PHARM' ;
declare variable $kfl:fid_bib := 'DE-84' ;
(: Inhaltsdatenlink siehe in marc:bd856 Funktion (auskommentiert) :)
declare variable $kfl:fid_url := 'http://fid-lizenzen.de' ;

(: kfl :)
declare variable $kfl:zdb_paketsigel := '' ;    (: nicht wenn nur eine einzige ZS :)
declare variable $kfl:zdb_id := '2026715-0' ;
declare variable $kfl:kfl_vertrags_id := 'prod_EACS_01' ;
declare variable $kfl:swets := '000376752' ;



(: =========================================== :)
(: functions                                   :)
(: =========================================== :)



declare function kfl:kfl912( $zdb-paketsigel )
as xs:string? {  
  if ( $zdb-paketsigel != '' )
  then
    concat(
      '912   ',
      '\031a',
      $zdb-paketsigel,
      "&#10;"
    )
  else
    trace( $zdb-paketsigel, 'Kein Paketsigel vorhanden! OK, wenn Einzelzeitschrift. ')
};



declare function kfl:kfl913( $swets )
as xs:string? {  
  if ($swets != '')
  then
  concat(
    '913   ',
    '\031a',   
      $swets,
    "&#10;"
  )
  else
    trace( $swets, 'Keine swets id angegeben!!')
};

declare function kfl:kfl929( $kfl-vertrags-id )
as xs:string? {  
  concat(
    '929   ',
    '\031a',
    if ($kfl-vertrags-id != '')
    then
      $kfl-vertrags-id
    else
      trace( $kfl-vertrags-id, 'Keine gültige $kfl-vertrags-id angegeben!!')
    ,
    "&#10;"
  )
};