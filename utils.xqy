(:+++++++++++++++++++++++++++++++++++++++++++++++++:)
(: JATS (Z39.96) Journal Archiving and Interchange :)
(: Conversion JATS to MARC21                       :)
(: @author: M. Kittelmann                          :)
(: Modul: Utils                                    :)
(:+++++++++++++++++++++++++++++++++++++++++++++++++:)

xquery version "1.0";
module namespace utils = "utils";


(: Cleaning Utils :)

declare function utils:clean-fname( $fname as xs:string?)
 as xs:string? {
   replace(
     replace(
       replace(
         replace( $fname, '\s+[ \.,\-]+$', ''),
         '\s*--\s*',
         ''
         ),
         ',\s*$',
         ''
     ),
   '([^\.]{2,})\.\s*$',
   '$1'
 )
 };
 

  declare function utils:clean-title ($title as xs:string? )
    as xs:string? {
      replace( $title, '\.$', '' )
  };
  
 
 (:  Other Utils :)
 
 declare function utils:if-absent
  ( $arg as item()* ,
    $value as item()* )  as item()* {

    if (exists($arg))
    then $arg
    else $value
 } ;
 
 declare function utils:replace-multi
  ( $arg as xs:string? ,
    $changeFrom as xs:string* ,
    $changeTo as xs:string* )  as xs:string? {

   if (count($changeFrom) > 0)
   then utils:replace-multi(
          replace($arg, $changeFrom[1],
                     utils:if-absent($changeTo[1],'')),
          $changeFrom[position() > 1],
          $changeTo[position() > 1])
   else $arg
 } ;
