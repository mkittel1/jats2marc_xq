(:+++++++++++++++++++++++++++++++++++++++++++++++++:)
(: JATS (Z39.96) Journal Archiving and Interchange :)
(: Conversion JATS to MARC21                       :)
(: @author: M. Kittelmann                          :)
(:+++++++++++++++++++++++++++++++++++++++++++++++++:)

import module namespace marc="marc" at "./marc.xqy";
import module namespace kfl="kfl" at "./kfl.xqy";
import module namespace u="utils" at "./utils.xqy";
declare namespace xlink="http://www.w3.org/1999/xlink" ;
declare namespace oai="http://www.openarchives.org/OAI/2.0/";
declare namespace oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/";
declare namespace dc="http://purl.org/dc/elements/1.1/";
declare namespace dcterms="http://purl.org/dc/terms/"; 
declare namespace ali="http://www.niso.org/schemas/ali/1.0";



for $x in /root()


(:++++++++++++++++++++:)
(: set values :)
(:++++++++++++++++++++:)

let $id := $x/article/front/article-meta/article-id[@pub-id-type="doi"]/text()

(: 008 :)
let $country := '|||'

let $from := ('EN', 'NL' )  (: codes present in file :)
let $to := ('eng', 'dut' )  (: MARC language codes :)
(: Es gibt zwar ein @lang, aber die Angabe scheint nicht zu stimmen ("de", aber Überschrift en) :)
let $language := 'eng'      (: oder besser '   ' ? :)
(: marc:replace-multi( $x/article/@xml:lang, $from, $to ) :)

let $year := $x/article/front/article-meta/pub-date[@pub-type="epub"]/year/text()

(: 02X :)
let $e_ISBN := ''
let $print_ISBN := '' 
let $e_ISSN :=  $x/article/front/journal-meta/issn[@pub-type="epub"]
let $print_ISSN := $x/article/front/journal-meta/issn[@pub-type="ppub"]
let $doi := $x/article/front/article-meta/article-id[@pub-id-type="doi"]/text()

(: 100 :)
let $first_author_inverted := if ( not( empty( $x/article/front/article-meta/contrib-group/contrib[@contrib-type="author"][1] ) ) ) then concat( $x/article/front/article-meta/contrib-group/contrib[@contrib-type="author"][1]/name/surname, ', ', $x/article/front/article-meta/contrib-group/contrib[@contrib-type="author"][1]/name/given-names ) else()

let $other_authors_inverted := for $y in $x/article/front/article-meta/contrib-group/contrib[@contrib-type="author" and position() > 1] return  concat( $y/name/surname, ', ', $y/name/given-names )

(: 245 :)
let $authors_normal_order := string-join( for $y in $x/article/front/article-meta/contrib-group/contrib[@contrib-type="author"] return concat( $y/name/given-names, ' ', $y/name/surname ), ', ' )
let $title := $x/article/front/article-meta/title-group/article-title
let $subtitle := $x/article/front/article-meta/title-group/subtitle

(: 260 :) (: nicht bei zs article :)
(: let $place_printing := $x/front/journal-meta/publisher/publisher-loc
let $publisher_name := $x/front/journal-meta/publisher/publisher-name :)

(: 773 :)
let $jtitle :=  concat( $x/article/front/journal-meta/journal-title, 
                        if ( $x/article/front/journal-meta/journal-subtitle )
                        then concat(
                        ' : ', 
                        $x/article/front/journal-meta/journal-subtitle )
                        else()
                      )
let $short_name :=  $x/article/front/journal-meta/journal-id[@journal-id-type = 'publisher-id']
let $volume :=  $x/article/front/article-meta/volume
let $issue := $x/article/front/article-meta/issue
(: $year s.o. #008 :)
(: let $day := $x/article/front/article-meta/pub-date[@pub-type="epub-ppub"]/day/text() :)  (: Tag nicht angegeben :)
let $month := $x/article/front/article-meta/pub-date[@pub-type="epub"]/month/text()
let $date := if ($year)
             then  if ($month)
                   then concat( $year, '-', $month )
                   else $year
             else()
  
let $fpage := $x/article/front/article-meta/fpage
let $lpage := $x/article/front/article-meta/lpage

let $page_from_page_to := if ($fpage and $lpage)
                          then if ($fpage = $lpage) then concat( 'p.', $fpage ) else concat( 'pp.', $fpage, '-', $lpage )
                          else if ($fpage)
                                then $fpage
                                else()
                                
                                
(: $gVol. 24, pt. B no. 9 (Sept. 1993), p. 235-48 :)
let $g :=   if ( $volume or $issue or $date or $page_from_page_to )
            then concat( 
                    if ($volume)
                    then concat( 'Vol. ', $volume )
                    else(),
                    if ($volume and $issue)
                    then  ', '
                    else(), 
                    if ($issue) 
                    then concat( 'Issue ', $issue )
                    else(), 
                    if ($date)
                    then concat( ' (', $date, ')' )
                    else(),
                    if ($page_from_page_to)
                    then concat( ', ', $page_from_page_to )
                    else()
              )
              else()

(: 774 :)
let $issue_title := $x/article/front/article-meta/issue-title
 
(: 856 :)
let $is_nationallizenz := false()
let $file := $x/article/front/article-meta/self-uri    
let $url := concat( 'http://doi.org/', $doi ) 

(: 520, 653 :)
let $keywords := $x/article/front/article-meta/kwd-group/kwd
let $abstract := string( $x/article/front/article-meta/abstract )  (: enthält html :)  

(: 542 :)
let $copyright_holder := $x/article/front/article-meta/permissions/copyright-holder
let $copyright_statement := $x/article/front/article-meta/permissions/copyright-statement
let $copyright-year := $x/article/front/article-meta/permissions/copyright-year
let $is_open_access := false()

(: kfl :)
(: cf. module kfl :)


(: ========================================================================:)
(: ========================= write MARC ===================================:)
(: ========================================================================:)

return
concat(
  marc:bdleader-zs(),
  marc:bd001( $id ),
  marc:bd003(),
  marc:bd005(),
  marc:bd008( $id, $year, $country, $language ),  
  
  marc:bd022( $id, $e_ISSN ),    
  marc:bd024( $id, $doi ),   
  marc:bd035( $id, $id, $kfl:supplier-org-code ), 
  marc:bd776( $id, $print_ISSN ), 
  
  marc:first-author( $id, $first_author_inverted ),
  marc:authors-700 ( $other_authors_inverted ),
    
  marc:bd245( $id, $title, $subtitle, $authors_normal_order ),
  marc:bd300( $id, $fpage, $lpage ),
  
  (: marc:bd500( $notes ), :)
  marc:bd520( $abstract ),
  marc:bd653( $keywords ),
 
  marc:bd773( $id, $jtitle, $short_name, $g, $volume, $issue, $fpage, $date, $e_ISSN ), 
  marc:bd774( $issue_title ), 
  marc:bd856( $id, $url, $kfl:fid_url, $kfl:proxy_url, $kfl:han, $kfl:fid_kennzeichen, $is_nationallizenz, '' ), 
  
  marc:bd542( $copyright_holder, $copyright_statement, $copyright-year, $is_open_access ),  
  
  (: Add KfL-specific fields :)   
  marc:bd016( $kfl:zdb_id ),
  marc:bd084( $kfl:fid_kennzeichen, $kfl:fid_bib ),   
  (: kfl:kfl912( $zdb_paketsigel ), :)   (: not for single journal :)
  kfl:kfl929( $kfl:kfl_vertrags_id ), 
  kfl:kfl913( $kfl:swets ), 
  
  "&#10;"  
)


