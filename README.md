# jats2marc_xq

Conversion JATS Journal Article Tag Suite to MARC21

The separations of concerns is implemented as follows:

<code>jats2marc.xq</code> contains variables and a boilerplate record query. The whole JATS-specific format logic is contained in the XPath assignment to the required variables. 

<code>marc.xqy</code> offers a function library that only knows how to write a MARC21 record (pre-ISO2709) when given the required values. 

<code>kfl.xqy</code> contains project-specific fields as well as all variables that require CONFIGURATION. 

<code>utils.xqy</code> contains additional functions mainly used for cleaning strings etc.

<code>\031</code> needs to be replaced by char dec. 031 in the final file. The file requires sorting by category before conversion to ISO2709.